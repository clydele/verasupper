<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\inventorycontroller;
use App\Http\Controllers\con_additem;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/','con_login');
//Route::get('/prod_history','con_prodhistory');
Route::get('/inventory', 'inventorycontroller@showInventory');
Route::resource('/admin_login','con_adminsign');
Route::resource('/store_stock','con_storeinventory');
Route::resource('/store_transactions','con_storetransactions');
Route::resource('/stores_sales','con_storeSales');
Route::resource('/additem','con_additem');
Route::resource('/purchase','con_purchase');
Route::resource('/pricelist','con_pricelist');
Route::resource('/payables','con_payables');  
Route::resource('/addTransaction','con_transactions');
Route::resource('/transProd','con_transProd');
Route::resource('/purchaseProd','con_purchaseProd');
Route::resource('/transAddProd','con_transAddProd');
Route::resource('/approval','con_approval');
Route::get('/approver','con_approver@showPOforApproval');
Route::post('/approver','con_approver@ApprovePO')->name('approver.approvePO');
Route::delete('/approver','con_approver@RejectPO')->name('approver.RejectPO');
Route::resource('/approved','con_approved');
Route::resource('/salesTransaction','con_salesTransaction');
Route::resource('/salesTransProd','con_salesTransProd');
Route::get('/viewSalesTrans','con_viewSalesTrans@showSalesTrans')->name('viewSalesTrans.view');
Route::get('/store_status','con_storeStatus@index')->name('storeStatus');
Route::resource('/signin','con_login');
Route::resource('/signout','con_signout');
Route::resource('/edit_po','con_poStatus');
Route::resource('/incomplete_po','con_incomplete');
Route::resource('/product_details','con_productDetails');

//added module for expense
Route::resource('/listExpense','con_expenselist');



//declared by Paola


Route::resource('/bucal_inventory','con_transactionsBucal');
Route::get('/bucal_storestock','inventorycontrollerBucal@showInventory');
Route::resource('/bucal_salestransaction','con_salesTransactionBucal');
Route::resource('/bucal_additem','con_additemBucal');
Route::resource('/bucal_pricelist','con_pricelistBucal');
Route::resource('/bucal_salesTransProd','con_salesTransProdBucal');
Route::resource('/bucal_transAddProd','con_transAddProdBucal');
Route::resource('/bucal_transProd','con_transProdBucal');
Route::get('/bucal_viewSalesTrans','con_viewSalesTransBucal@showSalesTrans')->name('bucal_viewSalesTrans.view');
Route::get('/bucal_viewSalesTrans','con_viewSalesTransBucal@showSalesTransTwo');
Route::resource('/bucal_addTransaction','con_transactionsBucal');

//central inventory
Route::get('/central_inventory','inventorycontrollercentral@showInventory');
Route::resource('/central_transactions','con_transactionsCentral');
Route::resource('/central_transAddProd','con_transAddProdCentral');






