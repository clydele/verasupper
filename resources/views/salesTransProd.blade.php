@extends('template')

@section('content')
<div class='container-fluid'>

        @if(strcmp(session('UserType'),'Bucal')!=0)
            <form method='POST'url='/salesTransProd'action="{{route('salesTransProd.store')}}">             
        @endif

        @if(strcmp(session('UserType'),'Bucal')==0)
        <form method='POST'url='/bucal_salesTransProd'action="{{route('bucal_salesTransProd.store')}}">  
        @endif
                
        @if(strcmp(session('UserType'),'Accountant')==0)
        <form method='POST'url='/central_salesTransProd'action="{{route('central_salesTransProd.store')}}">  
        @endif
    
    @csrf
    <div class='col-md-6'id='popout-modal'>
            <div class='modal-content'style='margin:auto auto;position:relative;'>
                <div class='modal-header'>Please fill the following information</div>
                <div class='modal-body'>
                    <div class='form-group'>
                       
                        <div class='row'>
                            <div class='col-md-6'>
                                <label for='tb_ProdCat'>Product Catalog</label>
                                <input type='text'name='tb_ProdCat'class='form-control'>
                            </div>
                            <div class='col-md-6'>
                                <label for='tb_ProdDesc'>Product Description</label>
                                <input type='text'name='tb_ProdDesc'class='form-control'>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-6'>
                                <label for='tb_lot'>Lot Number</label>
                                <input type='text'name='tb_lot' class='form-control'>
                            </div>
                            <div class='col-md-6'>
                                <label for='tb_Expiry'>Expiration</label>
                                <input type='text'name='tb_Expiry'class='form-control'>
                            </div>
                        </div>
                        <div class='row'>                                                          
                            <div class='col-md-3'>
                                <label for='tb_Quantity'>Quantity</label>
                                <input type='text'name='tb_Quantity'class='form-control'onchange='return computeTotal()'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_Unit'>Unit</label>
                                <input type='text'name='tb_Unit'class='form-control'>
                            </div>
                            <div class='col-md-3'>
                               <label for='tb_regularPrice'>Regular Price</label>
                                <input type='text'name='tb_regularPrice'class='form-control'>
                               
                            </div>
                            <div class='col-md-6'>
                                <label>Price Category</label>
                                <select class='form-control'onchange='return priceValue(this.value)'>
                                    <option>Regular</option>
                                    <option>Discounted</option>
                                </select>
                               
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_Price'>Price</label><label style='color:red'>*</label>
                                <input type='text'name='tb_Price'class='form-control'disabled>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_TPrice'>Total Price</label><label style='color:red'>*</label>
                                <input type='text'name='tb_TPrice'class='form-control'>
                            </div>
                            <div class='col-md-6'>
                            <label for='btn_submitTrans' style='color:red'>All fields marked with an asterisk (*) are required.</label>
                            </div>
                        </div>
                        
                        
                        <label for='btn_addproduct'>&nbsp;</label>
                        <button type='submit'name='btn_addProduct'class='btn btn-primary form-control'>Add Product</button>
                        <input type='hidden'name='tb_availQuantity'>
                    </div>
                </div>
                <div class='modal-footer'></div>
            </div>
            
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Add Product to Transaction</h2>
                
           
            </div>
            
          
           
            <div class='card-body'>
          
                
                @foreach($transDetail as $transDetails)          
                    
                    <div class='form-group'>
                        <div class='col-md-12'>
                            <div class='row'>
                            <div class='col-md-3'>
                                    <label for='dtp_salesDate'>Transaction Date & Time</label>
                                    <input type='text'name='tb_CashierName'class='form-control'value='{{$transDetails->created_at}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_SalesID'>Sales ID</label>
                                    <input type='text'name='tb_SalesID'class='form-control'value='{{$transDetails->SalesID}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_CName'>Customer</label>
                                    <input type='text'name='tb_CName'class='form-control'value='{{$transDetails->CName}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_Address'>Address</label>
                                    <input type='text'name='tb_Address'class='form-control'value='{{$transDetails->Address}}'>
                                </div>
                                
                            </div>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <label for='tb_Notes'>Notes</label>
                                    <input type='text'name='tb_Notes' placeholder='Notes' class='form-control'value='{{$transDetails->Notes}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_CashierName'>Cashier Name</label>
                                    <input type='text'name='tb_CashierName'class='form-control'value='{{$transDetails->CashierName}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_paymentMode'>Mode of Payment</label>
                                    <select name='tb_paymentMode'class='form-control'>
                                        <option selected disabled>-Please Choose One</option>
                                        <option>Cash</option>
                                        <option>Card</option>
                                        <option>Bank Transfer</option>
                                        <option>Cheque</option>
                                        <option>Gcash</option>
                                    </select>
                                </div>
                            </div>
                            <div class='row'>
                                
                                <div class='col-md-3'>
                                    <label for='tb_Vatable'>Vatable</label>
                                    <input type='text'name='tb_Vatable'class='form-control'value='{{$transDetails->Vatable}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_VAT'>VAT</label>
                                    <input type='text'name='tb_VAT'class='form-control'value='{{$transDetails->VAT}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_TotalSales'>Total Sales</label>
                                    <input type='text'name='tb_TotalSales'class='form-control'value='{{$transDetails->TotalSales}}'>
                                </div>
            </form>                    
                                <div class='col-md-3'>

                                @if(strcmp(session('UserType'),'Bucal')!=0)
                                    <form method='POST'action="{{url('salesTransaction',$transDetails->SalesID)}}">
              
                                @endif
                                @if(strcmp(session('UserType'),'Bucal')==0)
                                    <form method='POST'action="{{url('bucal_salestransaction',$transDetails->SalesID)}}">  
                                @endif
                                    
                                        @method('PUT')
                                        @csrf
                                        <label for='btn_Finish'>&nbsp;</label>
                                        <button type='submit'name='btn_Finish'class='form-control btn btn-primary'value='{{$transDetails->SalesID}}'>Finish</button>
                                    </form>
                                </div>    
                            </div>
                            
                        </div>
                    </div>  

                 @endforeach
    
                <div class='col-md-12'id='div_addedProd'style='overflow:scroll;height:300px;max-height:300px;'>
                    Added Products
                
                    <table class="table table-head-fixed">
                    <thead>
                        <th>Catalog</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Lot</th>
                        <th>Expiration</th>
                        <th>Price</th>
                        <th>Total Price</th>
                    </thead>
                    <tbody>
                        @foreach($addedProducts as $addedProduct)
                        <tr>
                            <td>{{$addedProduct->ProdCat}}</td>
                            <td>{{$addedProduct->ProdDesc}}</td>
                            <td>{{$addedProduct->Quantity}} {{$addedProduct->Unit}}</td>
                            <td>{{$addedProduct->Expiry}}</td>
                            <td>{{$addedProduct->lot}}</td>
                            <td>{{$addedProduct->Price}}</td>
                            <td>{{$addedProduct->TPrice}}</td>
                            <td>

                            @if(strcmp(session('UserType'),'Bucal')!=0)
                                <form method='POST'action="{{url('salesTransProd',$addedProduct->ProdCat.'~'.$addedProduct->SalesID.'~'.$addedProduct->salesProdNo)}}">
              
                            @endif
                            @if(strcmp(session('UserType'),'Bucal')==0)
                                <form method='POST'action="{{url('bucal_salesTransProd',$addedProduct->ProdCat.'~'.$addedProduct->SalesID.'~'.$addedProduct->salesProdNo)}}">
                            @endif

                            @if(strcmp(session('UserType'),'Accountant')==0)
                                <form method='POST'action="{{url('central_salesTransProd',$addedProduct->ProdCat.'~'.$addedProduct->SalesID.'~'.$addedProduct->salesProdNo)}}">
                            @endif
                            
                            @method('DELETE')
                            @csrf
                        
                            <input type='hidden'name='tb_aProdQuantity'>
                            <button type='submit'name='btn_remove'class='btn btn-primary'value="{{$addedProduct->ProdCat}}~{{$addedProduct->SalesID}}">Remove Product</button> 
                            </form>            
                            </td>
                        </tr>
                        @endforeach
                       
                    </tbody>
                    </table>
                </div>
                <div class='col-md-12'id='div_availProd'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <label for='tb_searchAvail'>Barcode Search</label>
                            <input type='text'class='form-control'name='tb_searchAvail'>
                        </div>
                        <div class='col-md-6'>
                            <label for='tb_availSearch'>Available Products</label>
                            <input type='text'name='tb_availSearch'list='InventoryList'class='form-control'>
                                <datalist id='InventoryList'>
                                    @foreach($availableProducts as $availableProd)
                                    <option>{{$availableProd->ProdCat}}~
                                        {{$availableProd->ProdDesc}}~
                                        {{$availableProd->Quantity}}~
                                        {{$availableProd->Unit}}~
                                        {{$availableProd->Category}}~
                                        {{$availableProd->lot}}~
                                        {{$availableProd->Expiry}}</option>
                                    @endforeach
                                </datalist>
                        </div>
                    </div>
                        Available Products
                        <table class="table table-head-fixed"style="max-height:500px;">
                            <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Description</th>
                                    <th>Available Quantity</th>
                                    <th>Unit</th>
                                    <th>Category</th>
                                    <th>Expiry</th>
                                    
                                </tr>
                            </thead>
                        
                            
                            <tbody>
                                @foreach($availableProducts as $availableProd)
                                    <tr>        
                                        <td>{{$availableProd->ProdCat}}</td>
                                        <td>{{$availableProd->ProdDesc}}</td>
                                        <td>{{$availableProd->Quantity}}</td>
                                        <td>{{$availableProd->Unit}}</td>
                                        <td>{{$availableProd->Category}}</td>
                                        <td>{{$availableProd->Expiry}}</td>
                                        <td><button type='button'name='btn_add'value='{{$availableProd->ProdCat}}*{{$availableProd->ProdID}}*{{$availableProd->ProdDesc}}*{{$availableProd->Quantity}}*{{$availableProd->Unit}}*{{$availableProd->Category}}*{{$availableProd->Expiry}}*{{$availableProd->lot}}*{{$availableProd->Regprice}}'onclick="return transQuantity(this.value);">Add</button></td>
                                        
                                    </tr>
                                @endforeach
                                <script>
                                function transQuantity(clicked)
                                    {
                                        document.getElementById("popout-modal").style.display="block";
                                        document.getElementById("div_availProd").style.display="none";
                                        document.getElementById("div_addedProd").style.display="none";
                                        document.getElementById("popout-modal").style.display="block";
                                        var buttondetails=clicked.split("*");
                                    
                                        document.getElementsByName("tb_ProdCat")[0].value=buttondetails[0];
                                        document.getElementsByName('tb_ProdDesc')[0].value=buttondetails[2];
                                        document.getElementsByName('tb_Quantity')[0].value=buttondetails[3].toString();
                                        document.getElementsByName('tb_Unit')[0].value=buttondetails[4].toString();
                                        document.getElementsByName('btn_addProduct')[0].value=buttondetails[1].toString();
                                        document.getElementsByName('tb_availQuantity')[0].value=buttondetails[3].toString();
                                        document.getElementsByName('tb_lot')[0].value=buttondetails[7].toString();
                                        document.getElementsByName('tb_Expiry')[0].value=buttondetails[6].toString();
										document.getElementsByName('tb_regularPrice')[0].value=buttondetails[8].toString();
                                        
                                    }


								  function computeTotal()
									{
											var num1=document.getElementsByName('tb_Quantity')[0].value
											var num2= document.getElementsByName('tb_Price')[0].value
											var total=num1*num2;
											document.getElementsByName('tb_TPrice')[0].value=total.toString();
									}
                                 function priceValue(clicked)
                                     {
                                         var price = parseFloat(document.getElementsByName('tb_regularPrice')[0].value.toString());
                                         var finalPrice=0;
                                         if(clicked=='Regular')
                                         {
                                             finalPrice=price
                                         }
                                         if(clicked=='Discounted')
                                         {
                                             finalPrice =(price/1.12)-((price/1.12)*.20);
                                         }
                                         if(clicked=='For Approval')
                                         {

                                         }
                                         document.getElementsByName('tb_Price')[0].value=finalPrice.toString();
                                         computeTotal();
                                     }
                                </script>  
                            </tbody>
                        </table>
                </div>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection
