<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{asset('login/fonts/icomoon/style.css')}}">

    <link rel="stylesheet" href="{{asset('login/css/owl.carousel.min.css')}}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('login/css/bootstrap.min.css')}}">
    
    <!-- Style -->
    <link rel="stylesheet" href="{{asset('login/css/style.css')}}">

    <title>VERA POS</title>
    <style>

        
    </style>
  </head>
  <body>
  

  <div class="d-lg-flex half">
  <div class="bg" style="background-image:url({{asset('login/images/background.PNG')}});"></div>
    <div class="contents order-2 order-md-1">
    

      <div class="container">
        <div class="row align-items-center justify-content-center" style='background-color:rgb(44,78,140)'>
        <img src="login/images/veraposlogo2.png" width="800px">
          <div class="col-md-7">
            <div class="mb-4">
              <h3 style='color:white'>Sign In as Employee</h3>
              <p class="mb-4">Welcome to VERA POS Hub</p>
            </div>
            
     
            <form action="/signin" method="POST"action="{{route('signin.store')}}"> 
              

   
            @csrf
              
              <div class="form-group mb-3" style="border:none">

                <input type='text'class='form-control'name='tb_startingBalance'placeholder='  Starting Balance'>
              </div>
              
              <div class="form-group mb-3" style="border:none">
                <input type="text" class="form-control" name="tb_username"placeholder='  Name'>

              </div>
              
              <div class="form-group mb-3" style="border:none">
                <input type="password" class="form-control" name="tb_password"placeholder='  Password'>
                
              </div>
           
          
            @if($Error=="1")
              <div class='alert alert-danger alert-dismissible'>
                Wrong Username or Password
              </div>
            @endif

              <input type="submit" value="Log In" class="btn btn-block btn-primary" style='background-color:rgb(146,208,80); color:rgb(44,78,140)'>
            </form>
            <br>
            <div>
              <label style='color:white'>Not an employee?</label>&nbsp;<a href='/admin_login'>Click here to sign in as admin</a>
            </div>
          </div>
        </div>
      </div>
    </div>

    
  </div>
    
    

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>