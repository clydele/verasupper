<html>
    <head>
        <title>TransactionPrint</title>
        <style>
            .page{width:8.5in;height:11in;position:relative;margin:auto auto;}
            .Details{width:98%;position:relative;margin:auto auto;text-align:center;font:12pt arial;}
                .Details table{width:100%;}
                .Details table tr{height:1cm;}
                .Details h2{color:green;background-color:#EEE8E7;border:1px solid green;text-align:center;}
            .Products{width:98%;position:relative;margin:auto auto;height:8in;}
                .Products table{width:100%;height:auto;max-height:100%;border:1px solid black;}
                .Products table tr td{text-align:center;}
                .Products h2{color:green;background-color:#EEE8E7;border:1px solid green;text-align:center;}
                .Products table th{color:green;font:16pt arial;height:0.5cm;border-bottom:1px solid green;}
                .Banner{width:100%;position:relative;margin:auto auto;height:auto;text-align:center;}
                
        </style>
    </head>
    <script>
        this.print();
    </script>
    <body>
    <div class='page'>
        <div class='Banner'>
            <h1>VERA MEDICAL SUPPLIES DEPOT</h1>
            <h2>Transaction Slip</h2>
        </div>
        <div class='Details'>
            
            <table>
                <tr>
                    <td colspan='3'><h2>Transaction Details</h2></td>
                </tr>
                @foreach($transDetails as $TransDetail)
                
                    
                    <tr>
                        <td><b>Transaction ID:</b>{{$TransDetail->transID}}</td>
                        <td></td>
                        <td><b>Date:</b>{{$TransDetail->created_at}}</td>
                    </tr>
                    <tr>
                        
                        <td><b>Type:</b>{{$TransDetail->transType}}</td>
                        <td><b>No. Of Items:</b>{{$TransDetail->noOfItems}}</td>
                        <td><b>Assisting Personnel:</b>{{$TransDetail->careTo}}</td>
                    </tr>
                    <tr>
                        <td colspan='3'rowspan='2'><b>Notes:</b>{{$TransDetail->Note}}</td>
                    </tr>
                
                @endforeach
            </table>
        </div>
        <div class='Products'>
            <table>
                <tr>
                    <td colspan='6'><h2>Transaction Products</h2></td>
                </tr>
                <tr>
                    <th>Product ID</th>
                    <th>Description</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Lot</th>
                    <th>Expiry</th>
                </tr>
                @foreach($TransactionProds as $transactionProd)
                    <tr>
                        <td>{{$transactionProd->prodId}}</td>
                        <td>{{$transactionProd->prodDesc}}</td>
                        <td>{{$transactionProd->quantity}}</td>
                        <td>{{$transactionProd->Unit}}</td>
                        <td>{{$transactionProd->lot}}</td>
                        <td>{{$transactionProd->Expiry}}</td>
                    </tr>
                    
                @endforeach
            </table>
        </div>
        <div class='Details'>
            <table>
                <tr>
                    <td>CHECKED BY:</td>
                    <td>_______________________</td>
                    <td>RECEIVED BY:</td>
                    <td>_______________________</td>
                </tr>

            </table>
        </div>
    </div>
    </body>
</html>