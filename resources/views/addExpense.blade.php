@extends('template')

@section('content')
<script>
    function discountCompute()
    {
        var price=document.getElementsByName('tb_regularPrice')[0].value;

        var discounted=price - (price*.20);
        document.getElementsByName('tb_discountedPrice')[0].value=discounted.toString();
    }
</script>
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-6'>
       
        @if(strcmp(session('UserType'),'Bucal')!=0)
            <form method='POST'url="/additems"action="{{ route('additem.store') }}"id="SaveItem">
              
        @endif
        @if(strcmp(session('UserType'),'Bucal')==0)
            <form method='POST'url="/additems"action="{{ route('bucal_additem.store') }}"id="SaveItem">  
        @endif
        
            <div class='card-header'>
                <h3 class='card-title'>Add a Product</h3>
            </div>           
               @csrf
                
            
            <div class='card-footer'>
                <div class='row'>
                    <div class='col-md-3'><input type='submit'name='btn_submit'value='Submit Product'class='btn btn-primary'></div>
                   
                    
                </div>
        </form>  
            </div>
        </div>
        <div class='col-md-6'>
            <div class='card-header'>
                <h3 class='card-title'>Product List</h3>
            </div>         
            <div class='card-body'>
            <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed dataTable">
                            <thead>
                                <tr>
                                    <th>Product ID</th>
                                    <th>Product Name</th>
                                    <th>Regular Price</th>
                                    <th>Discounted Price</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                           
                            @foreach($products as $product)
                                <tr>
                                    <td>{{$product->ProdCode}}</td>
                                    <td>{{$product->ProdName}}</td>
                                    <td>{{$product->Regprice}}</td>
                                    <td>{{$product->DiscPrice}}</td>                                   
                                    <td>

                                    @if(strcmp(session('UserType'),'Bucal')!=0)
                                        <form method="POST"action="{{url('additem',$product->ProductID)}}">
              
                                    @endif
                                    @if(strcmp(session('UserType'),'Bucal')==0)
                                        <form method="POST"action="{{url('bucal_additem',$product->ProductID)}}">  
                                    @endif
                                    

                                    @method('DELETE')
                                    @csrf
                                    <button type='submit'name='btn_delete'class='btn btn-primary'>Delete</button></td>
                                    </form>
                                </tr>
                            @endforeach
                            </tbody>
                            
                    </table>  
                </div>       
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>   
</div>

@endsection
