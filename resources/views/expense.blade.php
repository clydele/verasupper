@extends('template')

@section('content')

<div class='container-fluid'>
    <div class='row'>
       
        <div class='col-md-12'>
           
             <div class='card-body'>
                <div class='card-header'>
                    <h3>Actual Balance:{{number_format($CurrentBalance)}} </h3>
                </div>
                
                <form method='POST'url='/expenseList'action="{{route('listExpense.store')}}">
           
       
                <div class='form-group'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <h5>Cash In/ Cash Out</h5>
                        </div>
                    </div>
                    <div class='row'>
                        
                        <div class='col-md-6'>
                            <label for='tb_Description'>Description</label>
                            <input class='form-control' type='text'name='tb_Description'>
                        </div>
                        <div class='col-md-3'>
                            <label  for='tb_Staff'>Staff</label>
                            <input type='text'class='form-control'name='tb_Staff'>
                        </div>
                         <div class='col-md-3'>
                            <label  for='tb_Amount'>Amount</label>
                            <input type='text'class='form-control'name='tb_Amount'>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-3'>
                             <label  for='tb_EndingBalance'>Remaining Balance</label>
                             <input type='text'class='form-control'name='tb_remainingBal'>
                        </div>
                        <div class='col-md-3'>
                            <label for='cb_transType'>Transaction Type</label>
                            <select class='form-control'name='cb_transType'>
                                <option>Cash In</option>
                                <option>Cash Out</option>
                            </select>


                        </div>
                        <div class='col-md-3'></div>
                        <div class='col-md-3'>
                            <label >&nbsp;</label>
                            <input type='submit'class='form-control btn-primary'name='btn_saveLog'value='Cash Out'>
                        </div>
                    </div>
                </div>
                </form>
            </div>
             <div class='card-header'>
                <h3 class='card-title'>Expense Log </h3>
                
            </div>
            <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed dataTable">
                            <thead>
                                <tr>
                                    <th>DATE</th>
                                    <th>DESCRIPTION</th>
                                    <th>STAFF</th>
                                    <th>AMOUNT</th>
                                    <th>Running Balance</th>                                   
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($ExpenseLogs as $ExpenseLog)
                            
                                <tr>
                                    <td>{{$ExpenseLog->created_at}}</td>
                                    <td>{{$ExpenseLog->Description}}</td>
                                    <td>{{$ExpenseLog->Staff}}</td>
                                    <td>{{number_format($ExpenseLog->Amount)}}</td>
                                    <td>{{number_format($ExpenseLog->RunningBalance)}}</td>

                                </tr>
                            @endforeach
                            </tbody>
                            
                    </table>  
                </div>       
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>   
</div>

@endsection
