@extends('template')

@section('content')

<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Add Transaction</h2>
            </div>
            <div class='card-body'>

        @if(strcmp(session('UserType'),'Crossing')==0)
            <form method="POST"url="addTransaction"action="{{route('addTransaction.store')}}"> 
              
        @endif
        @if(strcmp(session('UserType'),'Bucal')==0)
        <form method="POST"url="addTransaction"action="{{route('bucal_addTransaction.store')}}">   
        @endif
        @if(strcmp(session('UserType'),'Accountant')==0)
        <form method="POST"url="addTransaction"action="{{route('central_transactions.store')}}">   
        @endif
        @csrf       
                <div class='form-group'>
                    <div class='row'>
                        <div class='col-md-3'>
                            <label for='tb_transID'>Transaction ID</label>
                            <input type='text'name='tb_transID' placeholder='ID No.' class='form-control'>               
                        </div>
                        <div class='col-md-3'>
                            <label for ='tb_transType'>Transaction type</label><label style='color:red'>*</label>
                            <select name='tb_transType'class='form-control'>
                                <option selected disabled>-Please select transaction type-</option>
                                <option>Stock In</option>
                                <option>Stock Out</option>
                                
                            </select>
                        </div>
                        <div class='col-md-3'>
                            <label for='tb_noOfItems'>Number of Items</label><label style='color:red'>*</label>
                            <input type='text'name='tb_noOfItems' placeholder='No. of Items' class='form-control'>
                        </div>
                        <div class='col-md-3'>
                            <label for='tb_careTo'>Customer/Personel</label><label style='color:red'>*</label>
                            <input type='text'name='tb_careTo' placeholder='Personnel Name' class='form-control'>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-9'>
                            <label for='rtb_note'>Notes</label>
                            <input type='text'name='rtb_note' placeholder='Notes' class='form-control'>
                        </div>
                        <div class='col-md-3'>
                            <label for='btn_submitTrans' style='color:red'>All fields marked with an asterisk (*) are required.</label>
                            <input type='submit'name='btn_createTrans'value='Create Transaction'class='form-control btn btn-primary'>   
                        </div>
                    </div>
                    
                </div>
                
                   

                                 
            </div>        
            <div class='card-footer'>
            
                
            </div>  
            </div>  
        </div>
          
           
    </form>       
     </div>       
            <div class='card-header'>
                <h2 class='card-title'>Inventory Transactions</h2>
            </div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                        
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>No. Of Items</th>
                                    <th>Care to</th>
                                    <th>Date Created</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach($TLogs as $transLogs)
                                    <tr>
                                        <td>{{$transLogs->transID}}</td>
                                        <td>{{$transLogs->transType}}</td>
                                        <td>{{$transLogs->noOfItems}}</td>
                                        <td>{{$transLogs->careTo}}</td>
                                        <td>{{$transLogs->created_at}}</td>
                                        <td>{{$transLogs->Note}}</td>
                                        <td>

                                        @if(strcmp(session('UserType'),'Crossing')==0)
                                        <form method="POST"action="{{url('addTransaction',$transLogs->transID)}}"> 
              
                                        @endif
                                        @if(strcmp(session('UserType'),'Bucal')==0)
                                        <form method="POST"action="{{url('bucal_addTransaction',$transLogs->transID)}}">   
                                        @endif
                                        @if(strcmp(session('UserType'),'Accountant')==0)
                                        <form method="POST"action="{{url('central_transactions',$transLogs->transID)}}">   
                                        @endif


                                            
                                            @method('PUT') 
                                            @csrf
                                            <input type='submit'name='btn_print'value='Print'>
                                            </form>
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
      
    </div>
         
    
    
  
</div>
@endsection