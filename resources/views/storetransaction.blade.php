@extends('template')

@section('content')
<div class='container-fluid'>
   
        <div class='col-md-12'>
            <div class='card-header'>
             
                    <div class='col-md-12'>
                        <div class='row'>
                             
                                <div class='col-md-3'>
                                    <form url="/store_transactions">
                                    @csrf
                                        <input type='submit'name='btn_submit'value='Vera Crossing'class=' form-control btn-primary'>
                                    </form>
                            
                                </div>
                           
                           
                                <div class='col-md-3'>
                                    <form method='POST'action="{{route('store_transactions.store')}}">
                                    @csrf
                                    <input type='submit'name='btn_submit'value='Vera Bucal'class='form-control  btn-primary'>
                                    </form>
                                </div>
                    
                            
                        </div>
                    </div>
                  
            
              
            </div>
            <div class='card-head'>
                    <div>
                        <h1>
                        @if($selectedStore=="")
                            Vera Crossing Inventory Transactions
                        @endif
                        @if($selectedStore=="Vera Bucal")
                            Vera Bucal Inventory Transactions
                        @endif
                        </h1>
                    </div>
                   
            </div>
            <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Type</th>
                                    <th>No. Of Items</th>
                                    <th>Care to</th>
                                    <th>Date Created</th>
                                    <th>Note</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach($TLogs as $transLogs)
                                    <tr>
                                        <td>{{$transLogs->transID}}</td>
                                        <td>{{$transLogs->transType}}</td>
                                        <td>{{$transLogs->noOfItems}}</td>
                                        <td>{{$transLogs->careTo}}</td>
                                        <td>{{$transLogs->created_at}}</td>
                                        <td>{{$transLogs->Note}}</td>
                                        <td>
                                            <form method="POST"action="{{url('store_transactions',$transLogs->transID)}}">
                                            @method('PUT') 
                                            @csrf
                                            @if($selectedStore=="")
                                               <input type='hidden'name='tb_store'value='Vera Crossing'>
                                            @endif
                                            @if($selectedStore=="Vera Bucal")
                                                <input type='hidden'name='tb_store'value='Vera Bucal'>
                                            @endif
                                            <input type='submit'name='btn_print'value='Print'class='btn-primary'>
                                            </form>
                                        </td>
                                        
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>  
                </div>     
               
                   
                
            
            <div class='card-footer'>
            </div>
        </div>
    
</div>
@endsection