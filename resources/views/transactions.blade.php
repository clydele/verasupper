@extends('template')

@section('content')
<div class='container-fluid'>
    <div class='col-md-12'>
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Inventory Transactions</h2>
            </div>
            <div class='card-body'>
                <input type='button'name='btn_addTrans'value='Add Transaction'class='btn btn-primary'>
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Transaction Type</th>
                            <th>No. Of Items</th>
                            <th>Customer/Personnel</th>
                            <th>Note</th>
                            
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($TLogs as $tlog)
                            <tr>
                                <td>{{$TLogs->transID}}</td>
                                <td>{{$TLogs->transType}}</td>
                                <td>{{$TLogs->noOfItems}}</td>
                                <td>{{$TLogs->careTo}}</td>
                                <td>{{$TLogs->Note}}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection