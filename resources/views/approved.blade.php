@extends('template')

@section('content')
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Approved Purchase Orders</h2>
            </div>
            <div class='card-body'>
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>Purchase #</th>
                            <th>Date</th>
                            <th>Supplier</th>
                            <th>Payment Terms</th>
                            <th>Vatable Sales</th>
                            <th>Total Sales</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($deliveredPOs as $deliveredPO)
                        <tr>
                            <td>{{$deliveredPO->PONumber}}</td>
                            <td>{{$deliveredPO->podate}}</td>
                            <td>{{$deliveredPO->Supplier}}</td>
                            <td>{{$deliveredPO->Terms}}</td>
                            <td>{{$deliveredPO->Vatable}}</td>
                            <td>{{$deliveredPO->TotalSales}}</td>                      
                            <td>
                                <input type='submit'name='btn_print'value='Print PO'>
                            </td>
                            <td>

                                <form method="POST"action="{{route('approved.update',$deliveredPO->PONumber)}}">

                                @method('PUT')
                                @csrf
                                <input type='submit'class='btn btn-primary'value='Edit Status'>
                                </form>
                            </td>
                            
                            
                            
                        </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                <table>
                   
                </table>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection