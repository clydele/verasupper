@extends('template')

@section('content')
<form method='post'> 
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>           
            <div class='card-header'></div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>PO Number</th>
                                    <th>Date</th>
                                    <th>Supplier</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
      
    </div>
         
    
    
</form>    
</div>
@endsection