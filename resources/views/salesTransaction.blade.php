@extends('template')

@section('content')

<div class='container-fluid'>
    
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Add Transaction</h2>
            </div>
            <div class='card-body'>
                
        @if(strcmp(session('UserType'),'Bucal')!=0)
            <form method="POST"url="salesTransaction"action="{{route('salesTransaction.store')}}">
              
        @endif
        @if(strcmp(session('UserType'),'Bucal')==0)
            <form method="POST"url="salesTransaction"action="{{route('bucal_salestransaction.store')}}">  
        @endif                
     
        @csrf       
                <div class='form-group'>
                    <div class='row'>
                        <div class='col-md-3'>
                            <label for='tb_transID'>Sales ID</label>
                            <input type='text'name='tb_transID' placeholder='Sales ID' class='form-control'>               
                        </div>
                        <div class='col-md-3'>
                            <label for ='tb_CName'>Customer Name</label><label style='color:red'>*</label>
                            <input type='text'name='tb_CName' placeholder='Customer Name' class='form-control'>
                        </div>
                        <div class='col-md-6'>
                            <label for='tb_Address'>Address</label><label style='color:red'>*</label>
                            <input type='text'name='tb_Address' placeholder='Address' class='form-control'>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6'>
                            <label for='rtb_Notes'>Notes</label>
                            <input type='text'name='rtb_Notes' placeholder='Notes' class='form-control'>
                        </div>
                        <div class='col-md-3'>
                            <label for='tb_CashierName'>Cashier</label><label style='color:red'>*</label>
                            <input type='text'name='tb_CashierName'placeholder='Cashier Name' class='form-control'>
                        </div>
                        <div class='col-md-3'>
                            <label for='btn_submitTrans' style='color:red'>All fields marked with an asterisk (*) are required.</label>
                            <input type='submit'name='btn_createTrans'value='Create Transaction'class='form-control btn btn-primary'>   
                        </div>
                    </div>
                    
                </div>
                
                   

                                 
            </div>        
            <div class='card-footer'>
            
                
            </div>  
            </div>  
        </div>
          
           
    </form>       
     </div>       
            <div class='card-header'>
                <h2 class='card-title'>Sales Transactions</h2>
            </div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>Sales ID</th>
                                    <th>Customer</th>
                                    <th>Cashier</th>
                                    <th>Vatable</th>
                                    <th>VAT</th>
                                    <th>Total Sales</th>
                                    <th>Date</th>
                                    <th>Notes</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach($TLogs as $transLogs)
                                    <tr>
                                        <td>{{$transLogs->SalesID}}</td>
                                        <td>{{$transLogs->CName}}</td>
                                        <td>{{$transLogs->CashierName}}</td>
                                        <td>{{$transLogs->Vatable}}</td>
                                        <td>{{$transLogs->VAT}}</td>
                                        <td>{{$transLogs->TotalSales}}</td>
                                        <td>{{$transLogs->created_at}}</td>
                                        <td>{{$transLogs->Notes}}</td>                                  
                                        <td>

                                        @if(strcmp(session('UserType'),"Bucal")!=0)
                                            <form method='GET'url='/viewSalesTrans'action="{{route('salesTransaction.edit',$transLogs->SalesID)}}">
                                        @endif
                                        @if(strcmp(session('UserType'),"Bucal")==0)
                                            <form method='GET'url='/bucal_viewSalesTrans'action="{{route('bucal_salestransaction.edit',$transLogs->SalesID)}}">
                                        @endif    
                                        @csrf
                                            <button type='submit'name='btn_view'value='{{$transLogs->SalesID}}'class='btn btn-primary'>View</button>
                                        
                                        @method('DELETE')
                                        @csrf
                                            <button type='submit'name='btn_delete'class='btn btn-primary'>Delete</button></td>
                                            </form>
                                            </form>
                                        </td>
                                        
                                        
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
      
    </div>
         
    
    
  
</div>
@endsection