@extends('template')

@section('content')

<div class='container-fluid'>
   
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Purchase Order</h2>
            </div>
            <div class='card-body'>
            @foreach($poDetails as $poDetail)
            
                 
                    <div class='form-group'>
            
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_PONumber'>PO Number</label>
                                <input type='text'name='tb_PONumber'class='form-control'value='{{$poDetail->PONumber}}'>                      
                            </div>
                            <div class='col-md-6'>
                                <label for='tb_supplier'>Supplier</label>
                                <input type='text'name='tb_supplier'class='form-control'value='{{$poDetail->Supplier}}'>
                            </div>
                            <div class='col-md-3'>
                                <label>&nbsp;</label>
                                <form method="POST"url='/approver'action="{{route('approver.approvePO')}}">
                                @csrf
                                    <button type='submit'class='btn btn-primary form-control'value='{{$poDetail->PONumber}}'name='btn_approve'>Approve</button>
                                </form>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_Vat'>VAT</label>
                                <input type='text'name='tb_Vat'class='form-control'value='{{$poDetail->Vat}}'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_Vatable'>Vatable</label>
                                <input type='text'name='tb_Vatable'class='form-control'value='{{$poDetail->Vatable}}'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_TotalSales'>Total Sales</label>
                                <input type='text'name='tb_TotalSales'class='form-control'value='{{$poDetail->TotalSales}}'>
                            </div>
                            <div class='col-md-3'>
                                <label>&nbsp;</label>
                                <form method='POST'url='/approver'action="{{route('approver.RejectPO')}}">
                                @csrf
                                @method('DELETE')
                                    <button type='submit'class='btn btn-danger form-control'value='{{$poDetail->PONumber}}'name='btn_reject'>Reject'</button>
                                </form>
                            </div>
                @endforeach            
                        </div>
                                        
                    </div>
             
            </div>
            <div class='card-header'>
                <h2 class='card-title'>Product Details</h2>
            </div>
            <div class='card-body'>
            
                </form>    
                                 
            </div>        
            <div class='card-footer'>
              
                
            </div>  
             
        </div>
          
           
      
     </div>
     
       
            
           
            <div class='card-header'></div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>Catalog</th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>                          
                            @foreach($poProds as $poProd)
                                <tr>
                                    <td>{{$poProd->pProdCat}}</td>
                                    <td>{{$poProd->pProdDesc}}</td>
                                    <td>{{$poProd->unit}}</td>
                                    <td>{{$poProd->quantity}}</td>
                                    <td>{{$poProd->price}}</td>
                                    <td>{{$poProd->tprice}}</td>
                                    <td>
                                        <form method='POST'action="{{url('purchaseProd',$poProd->pProd)}}">
                                                @method('DELETE')
                                                @csrf
                                               <button type='submit'name='btn_delete'class='btn btn-primary'value='{{$poProd->pProd}}'>Remove</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach           
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
    
         
    
    
  
</div>
@endsection