@extends('template')

@section('content')

<div class='container-fluid'>
   
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Purchase Order</h2>
            </div>
            <div class='card-body'>
            @foreach($poDetails as $poDetail)
              <form method="POST"action="{{url('purchase',$poDetail->PONumber)}}">
                  @method('PUT')
                   @csrf
                    <div class='form-group'>
            
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_PONumber'>PO Number</label>
                                <input type='text'name='tb_PONumber'class='form-control'value='{{$poDetail->PONumber}}'>                      
                            </div>
                            <div class='col-md-6'>
                                <label for='tb_supplier'>Supplier</label>
                                <input type='text'name='tb_supplier'class='form-control'value='{{$poDetail->Supplier}}'>
                            </div>
                            <div class='col-md-3'>
                                <label>&nbsp;</label>
                                <input type='submit'class='btn btn-primary form-control'value='Finish'>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_Vat'>VAT</label>
                                <input type='text'name='tb_Vat'class='form-control'value='{{$poDetail->Vat}}'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_Vatable'>Vatable</label>
                                <input type='text'name='tb_Vatable'class='form-control'value='{{$poDetail->Vatable}}'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_TotalSales'>Total Sales</label>
                                <input type='text'name='tb_TotalSales'class='form-control'value='{{$poDetail->TotalSales}}'>
                            </div>
                            
                        </div>
                                        
                    </div>
               </form>
            </div>
            <div class='card-header'>
                <h2 class='card-title'>Product Details</h2>
            </div>
            <div class='card-body'>
            <form method="POST"url="purchaseProd"action="{{route('purchaseProd.store')}}">
            @csrf        
                    <input type='hidden'name='tb_hPONumber'value='{{$poDetail->PONumber}}'>
            @endforeach  
                    <div class='form-group'>
                    @foreach($poDetails as $poDetail)
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_pProdCat'>Product Catalog</label>
                                <input type='text'name='tb_pProdCat'class='form-control'list='ProductList'onBlur="return prodSelected(this.value)"> 
                                    <datalist id='ProductList'>
                                        @foreach($ProdList as $ProdListItem)
                                           <option>{{$ProdListItem->ProdCode}}~{{$ProdListItem->ProdName}}~{{$ProdListItem->Unit}}</option>
                                        @endforeach                
                                    </datalist>                     
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_pProdDesc'>Name</label>
                                <input type='text'name='tb_pProdDesc'class='form-control'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_quantity'>Quantity</label>
                                <input type='text'name='tb_quantity'class='form-control'>
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_unit'>Unit</label>
                                <input type='text'name='tb_unit'class='form-control'>
                            </div>
                        </div>
                        <div class='row'>
                            <div class='col-md-3'>
                                <label for='tb_price'>Price per quantity</label>
                                <input type='text'name='tb_price'class='form-control'onChange="return autocompute(this.value)">
                            </div>
                            <div class='col-md-3'>
                                <label for='tb_tprice'>Total Price</label>
                                <input type='text'name='tb_tprice'class='form-control'>
                            </div>
        <script>
            function autocompute(value)
                {
                    var quantity=document.getElementsByName('tb_quantity')[0].value.toString();
                    var price=parseFloat(value)*parseFloat(quantity);
                    document.getElementsByName('tb_tprice')[0].value=price.toString();
                }
            function prodSelected(selected)
                {
                    
                    var buttondetails=selected.split("~");
                
                    document.getElementsByName('tb_pProdCat')[0].value=buttondetails[0].toString();
                    document.getElementsByName('tb_pProdDesc')[0].value=buttondetails[1].toString();
                    document.getElementsByName('tb_unit')[0].value=buttondetails[2].toString();
                    
                }
            
            </script> 
        <div class='col-md-3'>
                            </div>
                            <div class='col-md-3'>
                                <label for='btn_save'>&nbsp;</label>
                                <input type='submit'name='btn_save'value='Add Product'class='form-control btn btn-primary'>
                            </div>
                        </div>
                    @endforeach                      
                    </div>
                </form>    
                                 
            </div>        
            <div class='card-footer'>
              
                
            </div>  
             
        </div>
          
           
      
     </div>
     
       
            
           
            <div class='card-header'></div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>Catalog</th>
                                    <th>Description</th>
                                    <th>Unit</th>
                                    <th>Quantity</th>
                                    <th>Price</th>
                                    <th>Total Price</th>
                                </tr>
                            </thead>
                            <tbody>                          
                            @foreach($poProds as $poProd)
                                <tr>
                                    <td>{{$poProd->pProdCat}}</td>
                                    <td>{{$poProd->pProdDesc}}</td>
                                    <td>{{$poProd->unit}}</td>
                                    <td>{{$poProd->quantity}}</td>
                                    <td>{{$poProd->price}}</td>
                                    <td>{{$poProd->tprice}}</td>
                                    <td>
                                        <form method='POST'action="{{url('purchaseProd',$poProd->pProd)}}">
                                                @method('DELETE')
                                                @csrf
                                               <button type='submit'name='btn_delete'class='btn btn-primary'value='{{$poProd->pProd}}'>Remove</button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach           
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
    
         
    
    
  
</div>
@endsection