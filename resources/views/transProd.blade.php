@extends('template')

@section('content')
<div class='container-fluid'>

        @if(strcmp(session('UserType'),'Bucal')!=0))
            <form method='POST'url='/transProd'action="{{route('transProd.store')}}">
              
        @endif
        @if(strcmp(session('UserType'),'Bucal')==0)
        <form method='POST'url='/bucal_transProd'action="{{route('bucal_transProd.store')}}"> 
        @endif


    @csrf
    <div class='col-md-6'id='popout-modal'>
            <div class='modal-content'style='margin:auto auto;position:relative;'>
                <div class='modal-header'>Please fill the following information</div>
                <div class='modal-body'>
                    <div class='form-group'>
                        
                            <label for='tb_prodId'>Product Catalog</label>
                            <input type='text'name='tb_prodId'class='form-control'>
                            <label for='tb_prodDesc'>Product Description</label>
                            <input type='hidden'name='tb_availQuantity'>
                            <input type='text'name='tb_prodDesc'class='form-control'>
                            <label for='tb_Expiry'>Expiration</label>
                            <input type='text'name='tb_Expiry'class='form-control'>
                            <label for='tb_lot'>Lot Number</label>
                            <input type='text'name='tb_lot'class='form-control'>
                            <label for='tb_quantity'>Quantity</label>
                            <input type='text'name='tb_quantity'class='form-control'placeholder='please put number below available quantity for out transactions'>
                            <label for='tb_unit'>Unit</label>
                            <input type='text'name='tb_unit'class='form-control'placeholder='Unit'>
                            <label for='btn_addProduct'>Please fill all fields before submitting</label>
                            <button type='submit'name='btn_addProduct'class='form-control btn btn-primary'>Add Product</button>
                        
                    </div>
                </div>
                <div class='modal-footer'></div>
            </div>
            
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Add Product to Transaction</h2>
                
           
            </div>
            
          
           
            <div class='card-body'>
          
                
                @foreach($transDetail as $transactDetails)          
                    
                    <div class='form-group'>
                        
                        <label for='tb_transID'>Transaction ID</label>
                        <input type='text'name='tb_transID'value='{{$transactDetails->transID}}'>
                        <label for='tb_transType'>Type</label>
                        <input type='text'name='tb_transType'value='{{$transactDetails->transType}}'>
                        <label for='tb_person'>Person Assigned</label>
                        <input type='text'name='tb_person'value='{{$transactDetails->careTo}}'>
                        <label for='tb_date'>Date of Transaction</label>
                        <input type='text'name='tb_date'value='{{$transactDetails->created_at}}'>
                        <a href="/addTransaction"class='btn btn-primary'>Finish</a>
                       

                    </div>  

                 @endforeach
    </form>
                <div class='col-md-12'id='div_addedProd'style='overflow:scroll;height:300px;max-height:300px;'>
                    Added Products
                
                    <table class="table table-head-fixed">
                    <thead>
                        <th>Product Code</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Remove Product</th>
                    </thead>
                    <tbody>
                        @foreach($addedProducts as $addedProduct)
                        <tr>
                            <td>{{$addedProduct->prodId}}</td>
                            <td>{{$addedProduct->prodDesc}}</td>
                            <td>{{$addedProduct->quantity}}</td>
                            <td>
                            @if(strcmp(session('UserType'),'Bucal')!=0)
                                <form method='POST'action="{{url('transProd',$addedProduct->transProdNo)}}">
                                
                            @endif
                            @if(strcmp(session('UserType'),'Bucal')==0)
                                <form method='POST'action="{{url('bucal_transProd',$addedProduct->transProdNo)}}">
                            @endif
                            
                            @method('DELETE')
                            @csrf
                            
                           
                        
                            <input type='hidden'name='tb_aProdQuantity'value='{{$addedProduct->quantity}}'>
                            <button type='submit'name='btn_remove'class='btn btn-primary'value="{{$addedProduct->prodId}}">Remove Product</button> 
                            </form>            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                <div class='col-md-12'id='div_availProd'>
                        Available Products
                        <table class="table table-head-fixed"style="max-height:500px;">
                            <thead>
                                <tr>
                                    <th>ProdID</th>
                                    <th>ProdDesc</th>
                                    <th>Available Quantity</th>
                                    <th>Unit</th>
                                    <th>Lot Number</th>
                                    <th>Expiry</th>
                                    
                                </tr>
                            </thead>
                        
                            
                            <tbody>
                                @foreach($availableProducts as $availableProd)
                                    <tr>        
                                        <td>{{$availableProd->ProdCat}}</td>
                                        <td>{{$availableProd->ProdDesc}}</td>
                                        <td>{{$availableProd->Quantity}}</td>
                                        <td>{{$availableProd->Unit}}</td>
                                        <td>{{$availableProd->lot}}</td>
                                        <td>{{$availableProd->Expiry}}</td>
                                        <td><button type='button'name='btn_add'value='{{$availableProd->ProdCat}}*{{$availableProd->ProdID}}*{{$availableProd->ProdDesc}}*{{$availableProd->Quantity}}*{{$availableProd->Unit}}*{{$availableProd->Category}}*{{$availableProd->Expiry}}*{{$availableProd->lot}}'onclick="return transQuantity(this.value);">Add</button></td>
                                        
                                    </tr>
                                @endforeach
                                <script>
                                function transQuantity(clicked)
                                    {
                                        document.getElementById("popout-modal").style.display="block";
                                        document.getElementById("div_availProd").style.display="none";
                                        document.getElementById("div_addedProd").style.display="none";
                                        document.getElementById("popout-modal").style.display="block";
                                        var buttondetails=clicked.split("*");
                                    
                                        document.getElementsByName("tb_prodId")[0].value=buttondetails[0];
                                        document.getElementsByName('tb_prodDesc')[0].value=buttondetails[2].toString();
                                        document.getElementsByName('tb_quantity')[0].value=buttondetails[3].toString();
                                        document.getElementsByName('tb_unit')[0].value=buttondetails[4].toString();
                                        document.getElementsByName('btn_addProduct')[0].value=buttondetails[1].toString();
                                        document.getElementsByName('tb_availQuantity')[0].value=buttondetails[3].toString();
                                        document.getElementsByName('tb_Expiry')[0].value=buttondetails[6].toString();
                                        document.getElementsByName('tb_lot')[0].value=buttondetails[7].toString();
                                         
                                        
                                    }
                                
                                </script>  
                            </tbody>
                        </table>
                </div>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection