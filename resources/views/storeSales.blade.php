@extends('template')

@section('content')
<div class='container-fluid'>
   
        <div class='col-md-12'>
        <div class='card-header'>
             
             <div class='col-md-12'>
                 <div class='row'>
                      
                         <div class='col-md-3'>
                             <form url="/stores_sales">
                             @csrf
                                 <input type='submit'name='btn_submit'value='Vera Crossing'class=' form-control btn-primary'>
                             </form>
                     
                         </div>
                    
                    
                         <div class='col-md-3'>
                             <form method='POST'action="{{route('stores_sales.store')}}">
                             @csrf
                             <input type='submit'name='btn_submit'value='Vera Bucal'class='form-control  btn-primary'>
                             </form>
                         </div>
             
                     
                 </div>
             </div>
           
     
       
        </div>
        <div class='card-head'>
                    <div>
                        <h1>
                        @if($selectedStore=="Vera Crossing")
                            Vera Crossing Sales Transactions
                        @endif
                        @if($selectedStore=="Vera Bucal")
                            Vera Bucal Sales Transactions
                        @endif
                        </h1>
                    </div>   
        <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>Sales ID</th>
                                    <th>Customer</th>
                                    <th>Cashier</th>
                                    <th>Vatable</th>
                                    <th>VAT</th>
                                    <th>Total Sales</th>
                                    <th>Date</th>
                                    <th>Notes</th>
                                </tr>
                            </thead>
                            <tbody>                          
                                @foreach($TLogs as $transLogs)
                                    <tr>
                                        <td>{{$transLogs->SalesID}}</td>
                                        <td>{{$transLogs->CName}}</td>
                                        <td>{{$transLogs->CashierName}}</td>
                                        <td>{{$transLogs->Vatable}}</td>
                                        <td>{{$transLogs->VAT}}</td>
                                        <td>{{$transLogs->TotalSales}}</td>
                                        <td>{{$transLogs->created_at}}</td>
                                        <td>{{$transLogs->Notes}}</td>                                  
                                        <td>
                                            @if($selectedStore=="Vera Crossing")
                                                <form method='GET'url='/salesTransaction'action="{{route('salesTransaction.edit',$transLogs->SalesID)}}">
                                                    <button type='submit'name='btn_view'value='{{$transLogs->SalesID}}'class='btn btn-primary'>View</button>
                                                </form>
                                            @endif
                                            @if($selectedStore=="Vera Bucal")
                                                <form method='GET'url='/viewSalesTransBucal'action="{{route('bucal_salestransaction.edit',$transLogs->SalesID)}}">
                                                    <button type='submit'name='btn_view'value='{{$transLogs->SalesID}}'class='btn btn-primary'>View</button>
                                                </form>
                                            @endif
                                                
                                            
                                        </td>
                                        
                                        
                                       
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        </div>
    
</div>
@endsection