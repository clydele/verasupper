@extends('template')

@section('content')
<div class='container-fluid'>
   
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>View Sales Transaction</h2>
                
           
            </div>
            
          
           
            <div class='card-body'>
                {{session('selectedStore')}}
                
                @foreach($transDetail as $transDetails)          
                    
                    <div class='form-group'>
                        <div class='col-md-12'>
                            <div class='row'>
                            <div class='col-md-3'>
                                    <label for='dtp_salesDate'>Transaction Date & Time</label>
                                    <input type='text'name='tb_CashierName'class='form-control'value='{{$transDetails->created_at}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_SalesID'>Sales ID</label>
                                    <input type='text'name='tb_SalesID'class='form-control'value='{{$transDetails->SalesID}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_CName'>Customer</label>
                                    <input type='text'name='tb_CName'class='form-control'value='{{$transDetails->CName}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_Address'>Address</label>
                                    <input type='text'name='tb_Address'class='form-control'value='{{$transDetails->Address}}'>
                                </div>
                                
                            </div>
                            <div class='row'>
                                <div class='col-md-6'>
                                    <label for='tb_Notes'>Notes</label>
                                    <input type='text'name='tb_Notes'class='form-control'value='{{$transDetails->Notes}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_CashierName'>Cashier Name</label>
                                    <input type='text'name='tb_CashierName'class='form-control'value='{{$transDetails->CashierName}}'>
                                </div>
                                
                            </div>
                            <div class='row'>
                                
                                <div class='col-md-3'>
                                    <label for='tb_Vatable'>Vatable</label>
                                    <input type='text'name='tb_Vatable'class='form-control'value='{{$transDetails->Vatable}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_VAT'>VAT</label>
                                    <input type='text'name='tb_VAT'class='form-control'value='{{$transDetails->VAT}}'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_TotalSales'>Total Sales</label>
                                    <input type='text'name='tb_TotalSales'class='form-control'value='{{$transDetails->TotalSales}}'>
                                </div>
            </form>                    
                                <div class='col-md-3'>
                                   
                                </div>    
                            </div>
                            
                        </div>
                    </div>  

                 @endforeach
    
                <div class='col-md-12'id='div_addedProd'>
                    Added Products
                
                    <table class="table table-head-fixed">
                    <thead>
                        <th>Catalog</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total Price</th>
                    </thead>
                    <tbody>
                        @foreach($addedProducts as $addedProduct)
                        <tr>
                            <td>{{$addedProduct->ProdCat}}</td>
                            <td>{{$addedProduct->ProdDesc}}</td>
                            <td>{{$addedProduct->Quantity}} {{$addedProduct->Unit}}</td>
                            <td>{{$addedProduct->Price}}</td>
                            <td>{{$addedProduct->TPrice}}</td>
                            
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>    
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection