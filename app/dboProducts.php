<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboProducts extends Model
{
    protected $table = 'tbl_product';
    protected $primaryKey = 'ProductID';
}
