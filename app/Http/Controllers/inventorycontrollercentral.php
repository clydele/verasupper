<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Session;
use Redirect;

class inventorycontrollercentral extends Controller
{
    public function showInventory()
    {
      
        $leads = DB::select('select* from tbl_central_inventory WHERE Quantity !=0');
        return view('inventory',['leads'=>$leads]);
    }
}
