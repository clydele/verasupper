<?php

namespace App\Http\Controllers;

use App\dboPurchase;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;
use Redirect;

class con_approved extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $DelPOs=DB::select("select* from tbl_purchase WHERE Status='Approved' ORDER BY PONumber desc ");
        return view('approved',['deliveredPOs'=>$DelPOs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboPurchase  $dboPurchase
     * @return \Illuminate\Http\Response
     */
    public function show(dboPurchase $dboPurchase)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboPurchase  $dboPurchase
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        session(['EditedPONumber'=>$id]);
        return Redirect::to('edit_po');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboPurchase  $dboPurchase
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        session(['EditedPONumber'=>$id]);
        return Redirect::to('edit_po');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboPurchase  $dboPurchase
     * @return \Illuminate\Http\Response
     */
    public function destroy(dboPurchase $dboPurchase)
    {
        //
    }
}
