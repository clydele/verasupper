<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactions;
use App\dboTransProd;

use App\Inventory;
use Session;

class con_viewSalesTrans extends Controller
{
    public function showSalesTrans()
    {
    
       
        $transactionDetails=DB::select("select* from tbl_salestrans where SalesID='".session('viewedSalesTrans')."'");
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->SalesID;
        }
        $addedProds=DB::select("select* from tbl_salesprod WHERE SalesID='".$transID."'");
        return view('viewSalesTrans',['transDetail'=>$transactionDetails,'addedProducts'=>$addedProds]);
    }
    public function showSalesTransTwo()
    {
      
        $transactionDetails=DB::select("select* from tbl_salestrans where SalesID='".session('viewedSalesTrans')."'");
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->SalesID;
        }
        $addedProds=DB::select("select* from tbl_salesprod WHERE SalesID='".$transID."'");
        return view('viewSalesTrans',['transDetail'=>$transactionDetails,'addedProducts'=>$addedProds]);
    }
}
