<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboPurchase;
use Session;
use Redirect;


class con_purchase extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $POs=DB::select('select* from tbl_purchase');
        return view('purchase',['purchases'=>$POs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $order = new dboPurchase();
         $order->podate=$request->input('dtp_podate');
         $order->Supplier=$request->input('tb_supplier');
         $order->Address=$request->input('tb_address');
         $order->Terms=$request->input('tb_terms');
         $order->TelNo=$request->input('tb_telno');
         $order->MobileNo=$request->input('tb_mobile');
         $order->deliveryDate=$request->input('dtp_deldate');
         $order->Vatable="0";
         $order->Vat="0";
         $order->TotalSales="0";
         $order->Status="Created";
         $order->save();

         $ponumber="";
         $purchaseDetail=DB::select('select* from tbl_purchase ORDER BY PONumber desc Limit 1');
         foreach($purchaseDetail as $PODetails)
         {
             $ponumber=$PODetails->PONumber;
         }
         session(['EditedPONumber'=>$ponumber]);

         Session::flash('message', 'Successfully Created Purchase');
         return Redirect::to('purchaseProd');
  

                
                    
                
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $affectedRow=dboPurchase::find($id);
        $PONumber=$affectedRow->PONumber;
        $TotalPrice="";
        $AddedProducts=DB::select("select SUM(tprice) as TotalPrice from tbl_purchaseprod WHERE PONumber='".$PONumber."'");
        foreach($AddedProducts as $AddedProduct)
        {
            $TotalPrice=$AddedProduct->TotalPrice;
        }
        $TotalSales=$TotalPrice;
        $VAT=(int)$TotalPrice*0.12;
        $Vatable=$TotalPrice/1.12;
        

        $affectedRow->Vatable=$Vatable;
        $affectedRow->Vat=$VAT;
        $affectedRow->TotalSales=$TotalSales;
        $affectedRow->Status="For Approval";
        $affectedRow->save();
        
        return Redirect::to('purchaseProd');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows = dboPurchase::find($id);
        $affectedRows->delete();
        Session::flash('message', 'Successfully Deleted Purchase');
        return Redirect::to('purchase');
    }
}
