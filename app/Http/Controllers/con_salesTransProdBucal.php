<?php

namespace App\Http\Controllers;

use App\dboSalesTransProdBucal;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactionsBucal;
use App\dboTransProdBucal;

use App\InventoryBucal;
use Session;
use Redirect;

class con_salesTransProdBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $availableProds=DB::select('Select tbl_bucalinventory.*,tbl_bucalproduct.Regprice As Regprice from tbl_bucalinventory LEFT JOIN tbl_bucalproduct ON tbl_bucalproduct.ProdCode=tbl_bucalinventory.ProdCat AND tbl_bucalinventory.Quantity>0');
        $transactionDetails=DB::select('select* from tbl_bucalsalestrans Order By SalesID desc Limit 1');
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->SalesID;
        }
        $addedProds=DB::select("select* from tbl_bucalsalesprod WHERE SalesID='".$transID."'");
        return view('salesTransProd',['transDetail'=>$transactionDetails,'availableProducts'=>$availableProds,'addedProducts'=>$addedProds]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $TPs=new dboTransProdBucal;
        $TPs->TransID="S".$request->input('tb_SalesID');
        $TPs->transType="Purchase";
        $TPs->prodDesc=$request->input('tb_ProdDesc');
        $TPs->prodId=$request->input('tb_ProdCat');
        $TPs->quantity=$request->input('tb_Quantity');
        $TPs->Unit=$request->input('tb_Unit');
        $TPs->lot=$request->input('tb_lot');
        $TPs->Expiry=$request->input('tb_Expiry');
        $TPs->save();

        $STP=new dboSalesTransProdBucal;
        $STP->ProdCat=$request->input('tb_ProdCat');
        $STP->ProdDesc=$request->input('tb_ProdDesc');
        $STP->Quantity=$request->input('tb_Quantity');
        $STP->Unit=$request->input('tb_Unit');
        $STP->lot=$request->input('tb_lot');
        $STP->Expiry=$request->input('tb_Expiry');
        $STP->Price=$request->input('tb_Price');
        $STP->TPrice=$request->input('tb_TPrice');
        $STP->CName=$request->input('tb_CName');
        $STP->SalesID=$request->input('tb_SalesID');
        $STP->save();

        $Stocks=InventoryBucal::find($request->input('btn_addProduct'));
        $newQuantity=(int)$request->input('tb_availQuantity')-(int)$request->input('tb_Quantity');
        $Stocks->Quantity=$newQuantity;
        $Stocks->save();   
        return Redirect::to('bucal_salesTransProd');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dbouSalesTransProd  $dbouSalesTransProd
     * @return \Illuminate\Http\Response
     */
    public function show(dbouSalesTransProd $dbouSalesTransProd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dbouSalesTransProd  $dbouSalesTransProd
     * @return \Illuminate\Http\Response
     */
    public function edit(dbouSalesTransProd $dbouSalesTransProd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dbouSalesTransProd  $dbouSalesTransProd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dbouSalesTransProd $dbouSalesTransProd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dbouSalesTransProd  $dbouSalesTransProd
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $thrownDetails=explode("~",$id);
        $thrownID=$thrownDetails[2];
        $thrownCatalog=$thrownDetails[0];
        $thrownSalesID=$thrownDetails[1];
        $ProdQuantity="";   
        $ProdCat="";
        $StockQuantity="";
        $StockNum="";
        
        $affectedRow=dboSalesTransProdBucal::find($thrownID);
        $ProdCat=$affectedRow->ProdCat;
        $ProdQuantity=$affectedRow->Quantity;
        $ProdLot=$affectedRow->lot;
        $ProdExpiry=$affectedRow->Expiry;
        $affectedRow->delete();
       
            $affectedStock=DB::select("select* from tbl_bucalinventory WHERE ProdCat='".$ProdCat."' AND lot='".$ProdLot."' AND Expiry='".$ProdExpiry."'");
            if(sizeof($affectedRow)>0)
            {
                foreach($affectedStock as $affectedProd)
                {
                    $StockQuantity=$affectedProd->Quantity;
                    $StockNum=$affectedProd->ProdID;
                
                
             
                }
                $newQuantity=(int)$StockQuantity+(int)$ProdQuantity;
                $UpdatedRow=InventoryBucal::find($StockNum);
                $UpdatedRow->Quantity=$newQuantity;
                $UpdatedRow->save();
               
            }
         
       
        $affectedProd=DB::delete("Delete from tbl_bucaltransactionprod WHERE prodId='".$ProdCat."' and TransID='".$thrownSalesID."'");
        



        
        
        return Redirect::to('bucal_salesTransProd');
    
    }
}
