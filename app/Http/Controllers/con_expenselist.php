<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboExpense;
use App\dboBucalExpense;
use Session;
use Redirect;

class con_expenselist extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $BalanceNow=0;
    public function index()
    {
        $ExpenseLogs="";
        $ActualBalance="";
      if(strcmp(session('UserType'),"Crossing")==0)
      {
        
        $ExpenseLogs=DB::select("Select* from tbl_expense ORDER BY ExpenseNo desc");
        $ActualBalance=DB::select("Select* from tbl_expense ORDER BY ExpenseNo limit 1");
        foreach($ActualBalance as $record)
        {
            $BalanceNow=$record->RunningBalance;
        }

      }
      if(strcmp(session('UserType'),"Bucal")==0)
      {
            
        $ExpenseLogs=DB::select("Select* from tbl_bucalexpense ORDER BY ExpenseNo desc");
        $ActualBalance=DB::select("Select* from tbl_bucalexpense ORDER BY ExpenseNo limit 1");
        foreach($ActualBalance as $record)
        {
            $BalanceNow=$record->RunningBalance;
        }
      }
        return view('expense',['ExpenseLogs'=>$ExpenseLogs,'CurrentBalance'=>$BalanceNow]);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     if(strcmp(session('UserType'),"Crossing")==0)
     {
            $Expense=new dboExpense();
            $Expense->Description=$request->input('tb_Description');
            $Expense->Amount=$request->input('tb_Amount');
            $Expense->RunningBalance=$request->input('tb_RunningBalance');
            $Expense->Staff=$request->input('tb_Staff');
            $Expense->save();
     }
     if(strcmp(session('UserType'),"Bucal")==0)
     {
            $Expense=new dboBucalExpense();
            $Expense->Description=$request->input('tb_Description');
            $Expense->Amount=$request->input('tb_Amount');
            $Expense->RunningBalance=$request->input('tb_RunningBalance');
            $Expense->Staff=$request->input('tb_Staff');
            $Expense->save();
     }
      
        return Redirect::to('/listExpense');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
