<?php

namespace App\Http\Controllers;


use App\dboUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTimelog;
use App\dboTimelogBucal;
use Session;
use Redirect;

class con_login extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('signin',["Error"=>"0"]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $affectedRow=DB::select("select* from tbl_users WHERE Name='".$request->input('tb_username')."' AND Pword='".$request->input('tb_password')."'");
        if(sizeOf($affectedRow)>0)
        {
           
            $userType="";
            foreach($affectedRow as $affectedRecord)
            {
                $userType=$affectedRecord->Type;
            }
            session(["UserType"=>$userType]);

            if(strcmp($userType,"Crossing")==0)
            {
                
                $Timelogs=new dboTimelog();
                $Timelogs->Name=$request->input('tb_username');
                $Timelogs->TimeIn=Now();
                $Timelogs->StartingBalance=$request->input('tb_startingBalance');
                $Timelogs->save();

                $retreiveNextLogNum=DB::select("Select MAX(logID) as LastLog from tbl_timelog");
                $nextlog="";
                foreach( $retreiveNextLogNum as $rtls)
                {
                    $nextlog=$rtls->LastLog;
                }
                

                $LogId=$nextlog;
                session(["LogID"=>$LogId]);
                session(["UserName"=>$request->input('tb_username')]);
                return Redirect::to('/additem');

            }
            if(strcmp($userType,"Bucal")==0)
            {

               

                $Timelogs=new dboTimelogBucal();
                $Timelogs->Name=$request->input('tb_username');
                $Timelogs->TimeIn=Now();
                $Timelogs->StartingBalance=$request->input('tb_startingBalance');
                $Timelogs->save();
              
                $retreiveNextLogNum=DB::select("Select MAX(logID) as LastLog from tbl_bucaltimelog");
                $nextlog="";
                foreach( $retreiveNextLogNum as $rtls)
                {
                    $nextlog=$rtls->LastLog;
                }
                $LogId=$nextlog;
                session(["LogID"=>$LogId]);
                session(["UserName"=>$request->input('tb_username')]);
                return Redirect::to('/bucal_additem');
            }
           
            

            
          
        }
        else
        {
            
            return view('signin',["Error"=>"1"]);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboUsers  $dboUsers
     * @return \Illuminate\Http\Response
     */
    public function show(dboUsers $dboUsers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboUsers  $dboUsers
     * @return \Illuminate\Http\Response
     */
    public function edit(dboUsers $dboUsers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboUsers  $dboUsers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dboUsers $dboUsers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboUsers  $dboUsers
     * @return \Illuminate\Http\Response
     */
    public function destroy(dboUsers $dboUsers)
    {
        //
    }
}
