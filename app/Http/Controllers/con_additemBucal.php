<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboProductsBucal;
use Session;
use Redirect;
use App\InventoryBucal;

class con_additemBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=DB::select('select* from tbl_bucalproduct ORDER BY ProductID desc');
        return view('additem',['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

       
        $rules = array(
            'tb_prodID'       => 'required',
            'tb_prodDesc'      => 'required',
            'tb_supplier' => 'required',
            'tb_unit'   =>  'required',
            'tb_regularPrice'   =>'required',
            'tb_discountedPrice'    =>'required');
            
            $validator = Validator::make($request->input(), $rules);

            // process the login
            if ($validator->fails()) {
               echo"<script>alert('Please fill all fields');</script>";
            } else
             {
        
                    if(dboProductsBucal::where('ProdCode','=',$request->input('tb_prodID'))->exists())
                    {

                    }
                    else
                    {
                        $product = new dboProductsBucal();
                
                        $product->ProdCode= $request->input('tb_prodID');
                        $product->ProdName = $request->input('tb_prodDesc');    
                        $product->Unit  = $request->input('tb_unit');
                        $product->SupplierName=$request->input('tb_supplier');
                        $product->Regprice=$request->input('tb_regularPrice');
                        $product->DiscPrice=$request->input('tb_discountedPrice');
                        $product->save();
                    }

                   
                
    
                    
                    /*if(Inventory::where('ProdCat','=',$request->input('tb_prodID'))->exists())
                    {
                        
                    }
                    else
                    {
                        $Stocks = new Inventory();
                        $Stocks->ProdCat=$request->input('tb_prodID');
                        $Stocks->ProdDesc=$request->input('tb_prodDesc');
                        $Stocks->Quantity="0";
                        $Stocks->Unit=$request->input('tb_unit');
                        $Stocks->Category="VERA";
                        $Stocks->expiry="na";
                        $Stocks->save();
                    }*/
                  

                    // redirect
                    Session::flash('message', 'Successfully created product');
                    return Redirect::to('bucal_additem');
             
             }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
           
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows = dboProductsBucal::find($id);
        $affectedRows->delete();
        Session::flash('message', 'Successfully Deleted Purchase');
        return Redirect::to('bucal_additem');

        
    }
}
