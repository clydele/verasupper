<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Inventory;

class inventorycontroller extends Controller
{
    public function showInventory()
    {
      
        $leads = DB::select('select* from tbl_inventory WHERE Quantity !=0');
        return view('inventory',['leads'=>$leads]);
    }
}
