<?php

namespace App\Http\Controllers;

use App\dboTransProdCentral;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboCentralTransactions;
use App\InventoryCentral;
use App\dboProducts;
use Session;
use Redirect;

class con_transAddProdCentral extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $availableProds=DB::select('select* from tbl_central_inventory where Quantity>0');
        $ProdList=DB::select('select* from tbl_product');
        $transactionDetails=DB::select('select* from tbl_central_transaction Order By transID desc Limit 1');
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->transID;
        }
        $addedProds=DB::select("select* from tbl_central_transactionprod WHERE TransID='".$transID."'");
        return view('transAddProd',['transDetail'=>$transactionDetails,'availableProducts'=>$availableProds,'addedProducts'=>$addedProds,'ProdList'=>$ProdList]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $TPs=new dboTransProdCentral;
        $TPs->TransID=$request->input('tb_transID');
        $TPs->transType=$request->input('tb_transType');
        $TPs->prodDesc=$request->input('tb_prodDesc');
        $TPs->prodId=$request->input('tb_prodId');
        $TPs->quantity=$request->input('tb_quantity');
        $TPs->Unit=$request->input('tb_Unit');
        $TPs->lot=$request->input('tb_lot');
        $TPs->Expiry=$request->input('tb_expiry');
        $TPs->save();
        $inventoryQuantity="";
        $inventoryNum="";
        $Stocks=DB::select("select* from tbl_central_inventory WHERE ProdCat='".addslashes($request->input('tb_prodId'))."' AND lot='".addslashes($request->input('tb_lot'))."'");
        if(sizeof($Stocks)>0)
        {
            foreach($Stocks as $stock)
            {
                $inventoryQuantity=$stock->Quantity;
                $inventoryNum=$stock->ProdID;
            }
            $newQuantity=(int)$inventoryQuantity+(int)$request->input('tb_quantity');
            $updatedRow=InventoryCentral::find($inventoryNum);
            $updatedRow->Quantity=$newQuantity;
            $updatedRow->save();
            return Redirect::to('central_transAddProd');
        }
        else
        {
            $IIs=new InventoryCentral();
            $IIs->ProdCat=$request->input('tb_prodId');
            $IIs->ProdDesc=$request->input('tb_prodDesc');
            $IIs->Quantity=$request->input('tb_quantity');
            $IIs->Unit=$request->input('tb_Unit');
            $IIs->Category="";
            $IIs->Expiry=$request->input('tb_expiry');
            $IIs->lot=$request->input('tb_lot');
            $IIs->save();
            return Redirect::to('central_transAddProd');
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function show(dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function edit(dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ProdQuantity=""; 
        $ProdNum="";  
        $ProdCat="";
        $StockQuantity="";
        
        $affectedRow=dboTransProdCentral::find($id);
        $ProdCat=$affectedRow->prodId;
        $ProdQuantity=$affectedRow->quantity;
        $ProdLot=$affectedRow->lot;
        $ProdExpiry=$affectedRow->Expiry;
        $affectedRow->delete();

        $affectedStock=DB::select("select* from tbl_central_inventory WHERE ProdCat='".$ProdCat."' AND lot='".$ProdLot."'");
        if(sizeof($affectedStock)>0)
        {
            foreach($affectedStock as $affectedProd)
            {
                $StockQuantity=$affectedProd->Quantity;
                $ProdNum=$affectedProd->ProdID;
            }
            $newQuantity=(int)$StockQuantity-(int)$ProdQuantity;
            $updatedRow=InventoryCentral::find($ProdNum);
            $updatedRow->Quantity=$newQuantity; 
            $updatedRow->save();
        }
      
        return Redirect::to('central_transAddProd');
    }
}
