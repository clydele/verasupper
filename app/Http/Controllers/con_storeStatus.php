<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboPurchase;
use Session;
use Redirect;

class con_storeStatus extends Controller
{
    public function index()
    {

        $CrossingDay=DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_salesprod WHERE created_at LIKE '%".date('Y-m-d')."%'");
        $CrossingYesterday =DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_salesprod WHERE created_at LIKE '%".date('Y-m-d',strtotime("-1 days"))."%'");
        $CrossingTotalMonth=DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_salesprod WHERE created_at LIKE '%".date('Y-m')."%'");
        $CrossingTotalYear=DB::select("Select SUM(Quantity) As ItemSum,SUM(TPrice) as SumSales from tbl_salesprod WHERE created_at LIKE '%".date('Y-')."%'");
        $CrossingYesterdayStats=DB::select("Select MIN(StartingBalance) As StartBal,MAX(EndingBalance) As EndingBal from tbl_timelog WHERE created_at LIKE '%".date('Y-m-d',strtotime("-1 days"))."%'");

        $BucalDay=DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_bucalsalesprod WHERE created_at LIKE '%".date('Y-m-d')."%'");
        $BucalYesterday =DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_bucalsalesprod WHERE created_at LIKE '%".date('Y-m-d',strtotime("-1 days"))."%'");
        $BucalTotalMonth=DB::select("Select SUM(Quantity) AS ItemSum,SUM(TPrice) as SumSales from tbl_bucalsalesprod WHERE created_at LIKE '%".date('Y-m')."%'");
        $BucalTotalYear=DB::select("Select SUM(Quantity) As ItemSum,SUM(TPrice) as SumSales from tbl_bucalsalesprod WHERE created_at LIKE '%".date('Y-')."%'");
        $BucalYesterdayStats=DB::select("Select MIN(StartingBalance) As StartBal,MAX(EndingBalance) As EndingBal from tbl_bucaltimelog WHERE created_at LIKE '%".date('Y-m-d',strtotime("-1 days"))."%'");
        
        
        
       /* $TotalSales=DB::select("Select SUM(A.TotalSales) As Crossing, Sum(B.TotalSales) As Bucal from tbl_salestrans A, tbl_bucalsalestrans B WHERE A.created_at LIKE '%".date('Y-')."%' OR B.created_at LIKE '%".date('Y-')."%'");
        $Total="";
        foreach($TotalSales as $Totals)
        {
            $Total=$Totals->Crossing + $Totals->Bucal;
        }
        */
        $Total;
        foreach($BucalTotalYear as $BucalYearSales)
        {
            $Total= $BucalYearSales->SumSales;
        }
        foreach($CrossingTotalYear as $CrossingYear)
        {
            $Total +=$CrossingYear->SumSales;
        }
        $TotalPurchase=DB::select("Select SUM(TotalSales) AS TotalPO from tbl_purchase WHERE created_at LIKE '%".date('Y-')."%'");
        $TotalPurchaseAmount="";
        foreach($TotalPurchase as $TP)
        {
            $TotalPurchaseAmount=$TP->TotalPO;

        }
        
        return view('storeStatus',['TotalSales'=>$Total,'TotalPurchase'=>$TotalPurchaseAmount,'CrossingDay'=>$CrossingDay,'CrossingYesterday'=>$CrossingYesterday,'CrossingMonth'=>$CrossingTotalMonth,'CrossingYear'=>$CrossingTotalYear,'CrossingYS'=>$CrossingYesterdayStats,'BucalDay'=>$BucalDay,'BucalYesterday'=>$BucalYesterday,'BucalMonth'=>$BucalTotalMonth,'BucalYear'=>$BucalTotalYear,'BucalYS'=>$BucalYesterdayStats]);



    }
    
}
