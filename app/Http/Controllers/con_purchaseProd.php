<?php

namespace App\Http\Controllers;

use App\dboPurchaseProd;
use App\dboPurchase;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Session;
use Redirect;

class con_purchaseProd extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ponumber=session('EditedPONumber');
        $purchaseProds=DB::select("select* from tbl_purchaseprod WHERE PONumber='".$ponumber."'");
        $ProdListArray=DB::select("select* from tbl_product");
        $purchaseDetail=DB::select("select* from tbl_purchase WHERE PONumber='".$ponumber."'");
        return view('purchaseProd',['poDetails'=>$purchaseDetail,'poProds'=>$purchaseProds,'ProdList'=>$ProdListArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $POProduct=new dboPurchaseProd();
        $POProduct->pProdCat=$request->input('tb_pProdCat');
        $POProduct->pProdDesc=$request->input('tb_pProdDesc');
        $POProduct->unit=$request->input('tb_unit');
        $POProduct->quantity=$request->input('tb_quantity');
        $POProduct->price=$request->input('tb_price');
        $POProduct->tprice=$request->input('tb_tprice');
        $POProduct->PONumber=$request->input('tb_hPONumber');
        $POProduct->save();
        return Redirect::to('purchaseProd/');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboPurchaseProd  $dboPurchaseProd
     * @return \Illuminate\Http\Response
     */
    public function show(dboPurchaseProd $dboPurchaseProd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboPurchaseProd  $dboPurchaseProd
     * @return \Illuminate\Http\Response
     */
    public function edit(dboPurchaseProd $dboPurchaseProd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboPurchaseProd  $dboPurchaseProd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dboPurchaseProd $dboPurchaseProd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboPurchaseProd  $dboPurchaseProd
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRow=dboPurchaseProd::find($id);
        $affectedRow->delete();
        return Redirect::to('purchaseProd/');
    }
}
