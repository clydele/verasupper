<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTransactionsBucal extends Model
{
    protected $table="tbl_bucaltransaction";
    protected $primaryKey='transID';
}
