<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTransProd extends Model
{
    protected $table='tbl_transactionprod';
    protected $primaryKey='transProdNo';
}
