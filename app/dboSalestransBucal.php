<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboSalestransBucal extends Model
{
    protected $table='tbl_bucalsalestrans';
    protected $primaryKey='SalesID';
}
