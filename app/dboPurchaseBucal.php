<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboPurchaseBucal extends Model
{
    protected $table="tbl_bucalpurchase";
    protected $primaryKey='PONumber';
}
